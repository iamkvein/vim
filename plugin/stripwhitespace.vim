" Strip trailing whitespace
function! <SID>StripTrailingWhitespace()
  let _search=@/
  let l = line(".")
  let c = col(".")
  %s/\s\+$//e
  let @/=_search
  call cursor(l, c)
endfunction
noremap <Leader>ts :call <SID>StripTrailingWhitespace()<CR>
