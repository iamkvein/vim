"
" Conf: Fullscreen and distraction free writing
"

" Disable fullscreen on startup if not enabled already
if !exists("g:enable_fullscreen_at_startup")
  let g:enable_fullscreen_at_startup=0
endif

"
" Distraction free writing mode
" Don't forget to `$ defaults write org.vim.MacVim MMNativeFullScreen 0` to
" enable fullscreen bgcolor
"

function! DistractionFree()
  if exists("g:distractionfree_on") && g:distractionfree_on == 1
    set columns=160
    set ruler
    set number
    set relativenumber
    set nolinebreak
    if has("gui_running")
      set guioptions+=r
      set nofullscreen
    endif
    let g:distractionfree_on=0
  else
    set columns=110
    set noruler
    set nonumber
    set norelativenumber
    set linebreak
    if has("gui_running")
      set fuoptions=background:#00262330
      set guioptions-=r
      set fullscreen
    endif
    hi NonText guifg=#262330 cterm=NONE gui=NONE
    let g:distractionfree_on=1
  endif
endfunction

"
" Fullscreen
"

function! ToggleFullscreen()
  if &fullscreen == 1
   set guioptions+=r
   set nofullscreen
  else
   set guioptions-=r
   set fullscreen
  endif
endfunction

"
" Mappings
"

nmap <Leader>df :call DistractionFree()<CR>
nmap <Leader>fs :call ToggleFullscreen()<CR>

"
" Toggle fullscreen on startup
"

if has('gui') && g:enable_fullscreen_at_startup
  call ToggleFullscreen()
endif
