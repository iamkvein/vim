" Nalk Vim color file
" Maintainer: Kevin Faber <kevin.fab@gmail.com>
" Last Change: 02/03/2017
" Description: Brand new OFF colorz scheme

" ------------------------------------------------------------------------------
" Setup

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif

let g:colors_name="nalk"

" ------------------------------------------------------------------------------
" Colors

let s:gui_light_gray = '#ece9e1'
let s:gui_medium_gray = '#bab7c1'
let s:gui_medium_dark_gray = '#686b6a'
let s:gui_medium_darker_gray = '#383b3a'
let s:gui_dark_gray = '#1b1e1d'
let s:gui_red = '#f92672'
let s:gui_blue = '#03e2da'
let s:gui_blue_dark = '#093b39'
let s:gui_green = '#01e984'
let s:gui_purple = '#7572ff'

let s:cterm_light_gray = '252'
let s:cterm_medium_gray = '242'
let s:cterm_medium_dark_gray = '238'
let s:cterm_medium_darker_gray = '237'
let s:cterm_dark_gray = '236'
let s:cterm_red = '197'
let s:cterm_blue = '51'
let s:cterm_blue_dark = '31'
let s:cterm_green = '47'
let s:cterm_purple = '63'

" ------------------------------------------------------------------------------
" Definitions

exec "hi Normal guibg=". s:gui_dark_gray  ." guifg=". s:gui_medium_gray ." ctermbg=". s:cterm_dark_gray  ." ctermfg=". s:cterm_medium_gray
exec "hi Statement guibg=". s:gui_dark_gray  ." guifg=". s:gui_medium_gray ." ctermbg=". s:cterm_dark_gray  ." ctermfg=". s:cterm_medium_gray
exec "hi Operator guibg=". s:gui_dark_gray  ." guifg=". s:gui_medium_gray ." ctermbg=". s:cterm_dark_gray  ." ctermfg=". s:cterm_medium_gray
exec "hi Type guibg=". s:gui_dark_gray  ." guifg=". s:gui_medium_gray ." ctermbg=". s:cterm_dark_gray  ." ctermfg=". s:cterm_medium_gray
exec "hi PreProc guibg=". s:gui_dark_gray  ." guifg=". s:gui_medium_gray ." ctermbg=". s:cterm_dark_gray  ." ctermfg=". s:cterm_medium_gray
exec "hi Special guibg=". s:gui_dark_gray  ." guifg=". s:gui_medium_gray ." ctermbg=". s:cterm_dark_gray  ." ctermfg=". s:cterm_medium_gray
exec "hi SpecialKey guibg=". s:gui_dark_gray  ." guifg=". s:gui_medium_gray ." ctermbg=". s:cterm_dark_gray  ." ctermfg=". s:cterm_medium_gray
exec "hi Directory guibg=". s:gui_dark_gray  ." guifg=". s:gui_medium_gray ." ctermbg=". s:cterm_dark_gray  ." ctermfg=". s:cterm_medium_gray
exec "hi Question guibg=". s:gui_dark_gray  ." guifg=". s:gui_medium_gray ." ctermbg=". s:cterm_dark_gray  ." ctermfg=". s:cterm_medium_gray
exec "hi Underlined guibg=bg guifg=fg gui=none"

exec "hi Constant guibg=bg guifg=". s:gui_purple ." gui=bold ctermbg=bg ctermfg=". s:cterm_purple ." cterm=bold"
exec "hi Title guibg=bg guifg=". s:gui_purple ." gui=bold ctermbg=bg ctermfg=". s:cterm_purple ." cterm=bold"
exec "hi Identifier guibg=bg guifg=". s:gui_purple ." gui=none ctermbg=bg ctermfg=". s:cterm_purple ." cterm=none"

exec "hi Comment guibg=". s:gui_dark_gray  ." guifg=". s:gui_medium_dark_gray ." ctermbg=". s:cterm_dark_gray ." ctermfg=". s:cterm_light_gray
exec "hi Todo guibg=". s:gui_dark_gray ." guifg=". s:gui_light_gray ." gui=bold ctermbg=". s:cterm_light_gray . " ctermfg=". s:cterm_dark_gray

exec "hi NonText guibg=bg guifg=". s:gui_medium_dark_gray ." ctermbg=bg ctermfg=". s:cterm_medium_dark_gray
exec "hi EndOfBuffer guibg=bg guifg=". s:gui_medium_dark_gray ." ctermbg=bg ctermfg=". s:cterm_medium_dark_gray

exec "hi LineNr guibg=". s:gui_medium_darker_gray ." guifg=". s:gui_light_gray ." ctermbg=". s:cterm_medium_darker_gray ." ctermfg=". s:cterm_light_gray
exec "hi CursorLineNr guibg=". s:gui_medium_darker_gray ." guifg=". s:gui_light_gray ." ctermbg=". s:cterm_medium_darker_gray ." ctermfg=". s:cterm_light_gray
exec "hi SignColumn guibg=". s:gui_medium_darker_gray ." guifg=". s:gui_light_gray ." ctermbg=". s:cterm_medium_darker_gray ." ctermfg=". s:cterm_light_gray
exec "hi ColorColumn guibg=". s:gui_medium_darker_gray ." guifg=fg"

exec "hi LineNr guibg=". s:gui_dark_gray ." guifg=". s:gui_blue ." gui=bold ctermbg=". s:cterm_dark_gray ." ctermfg=". s:cterm_blue ." cterm=bold"
exec "hi CursorLineNr guibg=". s:gui_dark_gray ." guifg=". s:gui_blue ." gui=bold ctermbg=". s:cterm_dark_gray ." ctermfg=". s:cterm_blue ." cterm=bold"
exec "hi SignColumn guibg=". s:gui_dark_gray ." guifg=". s:gui_blue ." gui=bold ctermbg=". s:cterm_dark_gray ." ctermfg=". s:cterm_blue ." cterm=bold"

exec "hi TabLine guibg=". s:gui_dark_gray ." guifg=". s:gui_light_gray ." ctermbg=". s:cterm_dark_gray ." ctermfg=". s:cterm_light_gray
exec "hi TabLineSel guibg=". s:gui_light_gray ." guifg=". s:gui_dark_gray ." ctermbg=". s:cterm_light_gray ." ctermfg=". s:cterm_dark_gray
exec "hi TabLineFill guibg=". s:gui_medium_gray ." guifg=". s:gui_dark_gray ." ctermbg=". s:cterm_medium_gray ." ctermfg=". s:cterm_dark_gray

exec "hi VertSplit guibg=". s:gui_medium_darker_gray ." guifg=". s:gui_blue ." gui=none ctermbg=". s:cterm_medium_darker_gray ." ctermfg=". s:cterm_blue . " cterm=none"

exec "hi ErrorMsg guibg=". s:gui_red ." guifg=". s:gui_dark_gray ." ctermbg=". s:cterm_red ." ctermfg=". s:cterm_dark_gray
exec "hi Error guibg=". s:gui_dark_gray ." guifg=". s:gui_red ." ctermbg=". s:cterm_dark_gray ." ctermfg=". s:cterm_red
exec "hi WarningMsg guibg=". s:gui_light_gray ." guifg=". s:gui_red ." gui=bold ctermbg=". s:cterm_light_gray ." ctermfg=". s:cterm_red . " cterm=bold"
exec "hi MoreMsg guibg=bg guifg=fg gui=bold ctermbg=bg ctermfg=fg"

exec "hi Search guibg=". s:gui_blue ." guifg=". s:gui_dark_gray ." gui=none ctermbg=". s:cterm_blue ." ctermfg=". s:cterm_dark_gray ." cterm=none"
exec "hi IncSearch guibg=". s:gui_blue ." guifg=". s:gui_dark_gray ." gui=none ctermbg=". s:cterm_blue ." ctermfg=". s:cterm_dark_gray ." cterm=none"
exec "hi Visual guibg=". s:gui_blue_dark ." guifg=". s:gui_blue ." ctermbg=". s:cterm_blue_dark ." ctermfg=". s:cterm_blue
exec "hi MatchParen guibg=". s:gui_blue ." guifg=". s:gui_dark_gray ." ctermbg=". s:cterm_blue ." ctermfg=". s:cterm_dark_gray

exec "hi SpellBad gui=undercurl guisp=". s:gui_red ." ctermfg=". s:cterm_red
exec "hi SpellCap gui=undercurl guisp=". s:gui_purple ." ctermfg=". s:cterm_purple

exec "hi Cursor guibg=". s:gui_blue ." guifg=bg ctermbg=". s:cterm_blue ." ctermfg=bg"

exec "hi WildMenu guibg=". s:gui_light_gray ." guifg=". s:gui_dark_gray ." gui=bold ctermbg=". s:cterm_light_gray ." ctermfg=". s:cterm_dark_gray . " cterm=bold"

exec "hi Folded guibg=". s:gui_purple ." guifg=". s:gui_dark_gray ." ctermbg=". s:cterm_purple ." ctermfg=". s:cterm_dark_gray
exec "hi FoldColumn guibg=". s:gui_purple ." guifg=". s:gui_dark_gray ." ctermbg=". s:cterm_purple ." ctermfg=". s:cterm_dark_gray

exec "hi DiffAdd guibg=". s:gui_green ." guifg=bg ctermbg=". s:cterm_green ." ctermfg=bg"
exec "hi DiffChange guibg=". s:gui_blue ." guifg=bg ctermbg=". s:cterm_blue ." ctermfg=bg"
exec "hi DiffDelete guibg=". s:gui_red ." guifg=bg ctermbg=". s:cterm_red ." ctermfg=bg"
exec "hi DiffText guibg=". s:gui_purple ." guifg=bg ctermbg=". s:cterm_purple ." ctermfg=bg"

exec "hi PMenu guibg=". s:gui_green ." guifg=". s:gui_dark_gray ." ctermbg=". s:cterm_green ." ctermfg=". s:cterm_dark_gray
exec "hi PMenuSel guibg=". s:gui_light_gray ." guifg=". s:gui_green ." ctermbg=". s:cterm_light_gray ." ctermfg=". s:cterm_green
exec "hi PMenuSbar guibg=". s:gui_green ." guifg=". s:gui_dark_gray ." ctermbg=". s:cterm_green ." ctermfg=". s:cterm_dark_gray
exec "hi PMenuThumb guibg=". s:gui_green ." guifg=". s:gui_dark_gray ." ctermbg=". s:cterm_green ." ctermfg=". s:cterm_dark_gray

exec "hi EasyMotionTarget guibg=bg guifg=". s:gui_green ." gui=bold ctermbg=bg ctermfg=". s:cterm_green
exec "hi EasyMotionTargetDefault guibg=bg guifg=". s:gui_green ." ctermbg=bg ctermfg=". s:cterm_green

exec "hi StatusLine guibg=". s:gui_blue_dark ." guifg=". s:gui_blue ." gui=bold ctermbg=". s:cterm_blue_dark ." ctermfg=". s:cterm_blue ." cterm=bold"
exec "hi StatusLineNC guibg=". s:gui_blue_dark ." guifg=". s:gui_blue ." gui=bold ctermbg=". s:cterm_blue_dark ." ctermfg=". s:cterm_blue ." cterm=bold"

" ------------------------------------------------------------------------------
" Links

hi link Typedef Normal
hi link Structure Normal
hi link StorageClass Normal
hi link Conditional Normal
hi link Repeat Normal
hi link Keyword Normal
hi link Include Normal
hi link Define Normal
hi link Macro Normal
hi link PreCondit Normal
hi link Tag Normal
hi link SpecialChar Normal
hi link Delimiter Normal
hi link Debug Normal
hi link Exception Normal
hi link Label Normal
hi link Repeat Normal
hi link Function Normal
hi link Float Title
hi link Boolean Title
hi link Number Title
hi link Character Normal
hi link String Title
hi link multiple_cursors_cursor Cursor
hi link htmlEndTag Normal
hi link htmlTag Normal
hi link htmlTagName Identifier
hi link htmlTagSpecialName Identifier

