
" Don't care of old editions
set nocompatible

" Faster display
set ttyfast

" Don't want distraction
set noerrorbells
set visualbell

" Mouse can be used anywhere
"set mouse=a
" Hide mouse when typing
set mousehide

" Better encryption
if v:version > 743
  set cryptmethod=blowfish2
else
  set cryptmethod=blowfish
endif

" Set shell
if has('win32') || has('win64')
  set shell='cmd.exe'
else
  set shell=/bin/sh
endif

" Save when switching buffer
"set autowrite
" When a file is updated somewhere re-read it
set autoread
" Allow changing buffers without saving
set hidden
" Make VIM work seamlessly with the system clipboard on OSX
"set clipboard=unnamed

" Encoding
set encoding=utf-8
" Don’t think "Is my language supported?"
" The next time you `:set spell` VIM will download
" needed files if missing
set spelllang=fr

" Backup, swap
set directory=~/.vim/swap
if !isdirectory($HOME.'/.vim/swap')
  call mkdir($HOME.'/.vim/swap')
endif
set backupdir=~/.vim/backup
set backup
if !isdirectory($HOME.'/.vim/backup')
  call mkdir($HOME.'/.vim/backup')
endif

" Undo dir
set undofile
set undodir=~/.vim/undo
if !isdirectory($HOME.'/.vim/undo')
  call mkdir($HOME.'/.vim/undo')
endif

" Split window below current buffer on the right
set splitbelow
set splitright

" Tabs
set shiftwidth=2
set tabstop=2
set softtabstop=2
set expandtab
set smarttab
set shiftround

" Indent
set smartindent
set autoindent

" Allow backspacing on autoindent, line breaks and start of insert
set backspace=indent,eol,start

" No text wrapping by default
set nowrap

" Everythings wrap
set whichwrap=b,s,<,>,~,[,]
"             | | | | | | |_ ] Insert and Replace
"             | | | | | |___ [ Insert and Replace
"             | | | | |_____ ~ Normal
"             | | | |_______ <Right> Normal and Visual
"             | | |_________ <Left> Normal and Visual
"             | |___________ <Space> Normal and Visual
"

" Enable and set list chars
set list
"set listchars=tab:❯\ ,trail:❮,extends:▶,precedes:◀,nbsp:▮
set listchars=tab:·\ ,trail:·,extends:▶,precedes:◀,nbsp:▮

" Customize vertical split character
set fillchars=vert:˧,fold:–

" Autocompletion
set wildmode=list:longest,list:full
set wildignore+=*.o,*.obj,.git,*.rbc,*.class,*.pyc,.svn
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg
set wildignore+=*.DS_Store
set wildignore+=*.DS_Store
set wildignore+=**/bower_components/**,**/node_modules/**,**/vendor/**
set wildmenu
" Insert mode completion
set completeopt=menuone,menu,longest

" Folding
set nofoldenable " Disable by default
set foldmethod=syntax
set foldlevelstart=2
let javaScript_fold=1
let perl_fold=1
let php_folding=1
let ruby_fold=1
let sh_fold_enabled=1
let vimsyn_folding='af'
let xml_syntax_folding=1

" Incremental search like in modern browsers
set incsearch

" Case insensitive by default
set ignorecase
" But when CAPS do case sensitive
set smartcase
" RegEx magic
set magic

" We always want status line
set laststatus=2
" Always show line and col number and current command
set ruler
set showcmd
set showmode

" Line numbers
set number
" Relative line numbers
set relativenumber
" Good up to 99 999 lines
set numberwidth=6


" Always show sign column
set signcolumn=yes

" I don't want parents matching (delete this and set showmatch if you want to)
let loaded_matchparen=1
" If showmatch is on set how many tenth of seconds to blink matching bracket
" set matchtime=5

" Minimum number of lines above and below the cursor
set scrolloff=5
" Wen nowrap minimum of cols to keep left and right
set sidescrolloff=10

" Command area height
set cmdheight=2

" Font
set guifont=Cousine:h19
set linespace=4

" Hide MacVim toolbar
set go-=T

" Enable syntax
syntax on

" Set timeout
set timeoutlen=300
set ttimeoutlen=100

" Leader
let mapleader="\<Space>"
let maplocalleader="\<Space>"

"
" @status_line
"

" Get the size of current file
function! StatuslineFileSize()
  let bytes = getfsize(expand("%:p"))
  if bytes <= 0
    return ""
  endif
  if bytes < 1024
    return bytes
  else
    return (bytes / 1024) . "K"
  endif
endfunction

" Check if mixed-indentation or if indentation is different than expandtab setting
function! StatuslineTabWarning ()
  let tabs = search('^\t', 'nw') != 0
  let spaces = search('^ ', 'nw') != 0
  if tabs && spaces
    let b:statusline_tab_warning = ' !!tabs+spaces!! '
  elseif (spaces && !&et)
    let b:statusline_tab_warning = ' !!noexpand!! '
  elseif (tabs && &et)
    let b:statusline_tab_warning = ' !!expand!! '
  else
    let b:statusline_tab_warning = ''
  endif
  return b:statusline_tab_warning
endfunction

" Returns the syntax highlighting group under the cursor
function! SyntaxItem()
  return synIDattr(synID(line("."),col("."),1),"name")
endfunction

" 22) path.ext |unix,utf-8| l1:c1/150@0%
set statusline=\ %n)\ %f\ \|%{&ff}%{\",\".(&fenc==\"\"?&enc:&fenc)}\%{(&filetype==\"\ \"?&ft:\"\")}\|
"                |    |     |
"                |    |     |______________________________________________________ current file encoding
"                |    |____________________________________________________________ current file format
"                |__________________________________________________________________ filename (%F for full path to file)

set statusline+=\ l%l:c%c/%L@%p%%
"                 |  |  |  |
"                 |  |  |  |______ position in buffer
"                 |  |  |_________ number of lines
"                 |  |____________ current column
"                 |_______________ current line

set statusline+=\ %m%r%h%w
"                 | | | |
"                 | | | |_________________________________________ preview flag in square bracket
"                 | | |___________________________________________ help flag in square bracket
"                 | |_____________________________________________ readonly flag in square bracket
"                 |_______________________________________________ modified flag in square bracket

" Syntax higlighting group
set statusline+=\ %{SyntaxItem()}

" Put what follows on the right
set statusline+=%=
" Tab warning
set statusline+=%#error#%{StatuslineTabWarning()}%*

"
" @mappings
"

" Disable MAN page mapping
noremap <S-k> <Nop>

" Hate that stupid window
noremap q: :q

" Quit
nnoremap <Leader>q :q<Cr>
nnoremap <Leader>Q :q!<Cr>

" Shortcut to edit .vimrc
nnoremap <Leader>rc :e $MYVIMRC<CR>
" Shortcut to source .vimrc
nnoremap <Leader>cr :so $MYVIMRC<CR>

" Save current buffer with root privileges
nnoremap <Leader>w! :w !sudo tee %<CR>

" Switch visual mode to block
nnoremap <Leader>vb :set ve=block<CR>
" Switch visual mode to all
nnoremap <Leader>va :set ve=all<CR>
" Switch visual mode back to default
nnoremap <Leader>vd :set ve=<CR>

" Toggle paste
nnoremap <Leader>pt :set invpaste<CR>
vnoremap <Leader>pt <Esc>:set invpaste<CR>

" Toggle scrollbind
nnoremap <Leader>sb :set invscrollbind<CR>
vnoremap <Leader>sb <Esc>:set invscrollbind<CR>

" Folding
nnoremap <Leader>ft Vatzf

" Fast tab navigation
nnoremap <Leader>th :tabprev<CR>
nnoremap <Leader>tl :tabnext<CR>
nnoremap <Leader>tk :tabfirst<CR>
nnoremap <Leader>tj :tablast<CR>
nnoremap <Leader>tr <C-W><C-W>
" Fast buffer navigation
nnoremap <Leader>ih :bprev<CR>
nnoremap <Leader>il :bnext<CR>
nnoremap <Leader>ik :bfirst<CR>
nnoremap <Leader>ij :blast<CR>
" Fast location list navigation
nnoremap <Leader>eh :lprevious<CR>
nnoremap <Leader>el :lnext<CR>
nnoremap <Leader>ek :lfirst<CR>
nnoremap <Leader>ej :llast<CR>
" Fast quickfix navigation
nnoremap <Leader>ch :cprevious<CR>
nnoremap <Leader>cl :cnext<CR>
nnoremap <Leader>ck :cfirst<CR>
nnoremap <Leader>cj :clast<CR>
nnoremap <Leader>cj :clast<CR>
nnoremap <Leader>cn :cnfile<CR>

" Toggle search highlight
function! ToggleSearchHighlight()
  if (&hls == 0)
    set hls
  else
    set nohls
  endif
endfunction
nnoremap <Leader>h :call ToggleSearchHighlight()<CR>

" Arghh! Hitting that key too fast.
cnoremap W w
cnoremap Q q

" Vertical help
cnoremap vh vertical help

" Select last changed (or pasted) text
nnoremap gp `[v`]
" Shifts keep selection
vnoremap < <gv
vnoremap > >gv

" Avoid loss of text when <C-u> or <C-w>
inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>

" Reflow paragraph
nnoremap F gqap
" Reflow visually highlighted lines
vnoremap F gq

" Digraphs
"" α Alpha
imap <c-l><c-a> <c-k>a*
"" β Beta
imap <c-l><c-b> <c-k>b*
"" γ Gamma
imap <c-l><c-g> <c-k>g*
"" δ Delta
imap <c-l><c-d> <c-k>d*
"" ε Epslion
imap <c-l><c-e> <c-k>e*
"" λ Lambda
imap <c-l><c-l> <c-k>l*
"" η Eta
imap <c-l><c-y> <c-k>y*
"" θ Theta
imap <c-l><c-h> <c-k>h*
"" μ Mu
imap <c-l><c-m> <c-k>m*
"" ρ Rho
imap <c-l><c-r> <c-k>r*
"" π Pi
imap <c-l><c-p> <c-k>p*
"" φ Phi
imap <c-l><c-f> <c-k>f*

"
" @abbrevs
"

" I find myself using vsp more than sp
cabbrev sp vsp
cabbrev hsp sp

" Common french mistakes
iabbrev acceuil accueil
iabbrev parrallèle paralléle
iabbrev paralèlle paralléle

"
" @packages
"

packadd! matchit

"
" @plugins
"

call plug#begin('~/.vim/plugged')

if has('python3')
  Plug 'roxma/vim-hug-neovim-rpc'
  Plug 'roxma/nvim-yarp', { 'do': 'pip install -r requirements.txt'  }
  Plug 'Shougo/denite.nvim'
  Plug 'chemzqm/denite-git'
  Plug 'Shougo/neomru.vim'
endif

Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-jdaddy'

Plug 'kana/vim-textobj-user'
Plug 'kana/vim-textobj-line'
Plug 'kana/vim-textobj-indent'
Plug 'Julian/vim-textobj-brace'
Plug 'lucapette/vim-textobj-underscore'
Plug 'saaguero/vim-textobj-pastedtext'
Plug 'akiyan/vim-textobj-php', { 'for': 'php' }
Plug 'tyru/open-browser.vim'

Plug 'dyng/ctrlsf.vim'
Plug 'easymotion/vim-easymotion'
Plug 'haya14busa/vim-asterisk'
Plug 'PeterRincker/vim-argumentative'
Plug 'gcmt/wildfire.vim'
Plug 'ervandew/supertab'

Plug 'tpope/vim-eunuch'
Plug 'mhinz/vim-signify'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'

Plug 'neovimhaskell/haskell-vim', { 'for': ['haskell', 'tidal'] }
Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
Plug 'plasticboy/vim-markdown'
Plug 'StanAngeloff/php.vim', { 'for': 'php' }
Plug 'othree/html5.vim', { 'for': 'html' }
Plug 'rust-lang/rust.vim'
Plug 'alvan/vim-closetag'
Plug 'scrooloose/syntastic', { 'for': ['javascript', 'css', 'html', 'php', 'rust'] }
Plug 'lumiliet/vim-twig'
Plug 'cakebaker/scss-syntax.vim'
Plug 'munshkr/vim-tidal'

Plug 'tomtom/tcomment_vim'
Plug 'raimondi/delimitmate'
Plug 'mattn/emmet-vim'
Plug 'junegunn/vim-easy-align'
Plug 'AndrewRadev/switch.vim'
Plug 'AndrewRadev/splitjoin.vim'

if has('mac')
  Plug 'darfink/vim-plist'
endif

call plug#end()

"
" @plugin_settings
"

"""
" Asterisk
map *   <Plug>(asterisk-*)
map #   <Plug>(asterisk-#)
map g*  <Plug>(asterisk-g*)
map g#  <Plug>(asterisk-g#)
map z*  <Plug>(asterisk-z*)
map gz* <Plug>(asterisk-gz*)
map z#  <Plug>(asterisk-z#)
map gz# <Plug>(asterisk-gz#)

"""
" DelimitMate
let delimitMate_expand_cr = 1
let delimitMate_expand_space = 1

"""
" Easyalign
" Like vim smartcase
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

"""
" Easymotion
" Like vim smartcase
let g:EasyMotion_smartcase = 1
" N-char search
" map  / <Plug>(easymotion-sn)
" omap / <Plug>(easymotion-tn)
" map  n <Plug>(easymotion-next)
" map  N <Plug>(easymotion-prev)
" Jump to anywhere
map <Leader>e <Plug>(easymotion-prefix)
map é  e<Plug>(easymotion-jumptoanywhere)

"""
" Emmet
let g:user_emmet_leader_key = '<C-e>'

"""
" Markdown
let g:vim_markdown_toc_autofit = 1

""
" Disable NetRW banner
let g:netrw_banner=0

"""
" Open browser
let g:netrw_nogx = 1
nmap gx <Plug>(openbrowser-smart-search)
vmap gx <Plug>(openbrowser-smart-search)

"""
" Syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 1
let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': [],'passive_filetypes': [] }
let g:syntastic_html_checkers = ['tidy']
let g:syntastic_html_tidy_exec = "/usr/local/bin/tidy"
let g:syntastic_css_checkers = ['prettycss']
let g:syntastic_css_prettycss_args = "--ignore=suggest-relative-unit --ignore=browser-unsupported --ignore=browser-quirk"
let g:syntastic_javascript_checkers = ['jshint']
let g:syntastic_php_checkers = ['php', 'phpmd']
let g:syntastic_php_phpcs_args = "--standard=Zend"
nmap <Leader>st :SyntasticToggleMode<CR>

"""
" Denite
" Configuration
if has('python3')
  call denite#custom#var('file/rec', 'command',
    \ ['ag', '--follow', '--nocolor', '--nogroup',
    \ '--ignore', 'node_modules', '-g', ''])
  call denite#custom#var('directory_rec', 'command',
    \ ['find', ':directory', '-type', 'd',
    \ '!', '-path', '*.git*',
    \ '!', '-path', '*node_modules*',
    \ '!', '-path', '*bower_components*'])
  " Mappings
  nnoremap <Leader>d :Denite -split=no -buffer-name=dedirectories directory_rec<cr>
  nnoremap <Leader>. :Denite -split=no -buffer-name=defiles file/rec<cr>
  nnoremap <Leader>b :Denite -split=no -buffer-name=debuffers -no-empty buffer<cr>
  nnoremap <Leader>o :Denite -split=no -buffer-name=deoutline outline<cr>
  nnoremap <Leader>/ :Denite -split=no -buffer-name=degrep grep<cr>
  nnoremap <Leader>l :Denite -split=no -buffer-name=delines line<cr>
  nnoremap <Leader>u :Denite -split=no -buffer-name=defiles file_mru<cr>
  nnoremap <Leader>s :Denite -split=no -buffer-name=degit gitstatus<cr>
  autocmd FileType denite call s:denite_my_settings()
  function! s:denite_my_settings() abort
    nnoremap <silent><buffer><expr> <CR>
    \ denite#do_map('do_action')
    nnoremap <silent><buffer><expr> d
    \ denite#do_map('do_action', 'delete')
    nnoremap <silent><buffer><expr> p
    \ denite#do_map('do_action', 'preview')
    nnoremap <silent><buffer><expr> q
    \ denite#do_map('quit')
    nnoremap <silent><buffer><expr> i
    \ denite#do_map('open_filter_buffer')
    nnoremap <silent><buffer><expr> <Space>
    \ denite#do_map('toggle_select').'j'
  endfunction
endif

"""
" CtrlSF
" Configuration
let g:ctrlsf_auto_focus = {
    \ "at": "start"
    \ }
" Mappings
nmap <C-t>s <Plug>CtrlSFPrompt
nnoremap <C-t>t :CtrlSFToggle<CR>
inoremap <C-t>t <Esc>:CtrlSFToggle<CR>

"
" @colorscheme
"

let &colorcolumn="80,".join(range(120,999),",")

if $TERM_PROGRAM =~ "iTerm"
  set t_ut=
  set termguicolors
  set background=
  color nalk
elseif has('gui_running')
  set background=dark
  " Default window size
  set lines=60 columns=100
  " Current line highlighting
  set cursorline
  color outrun
else
  set t_ut=
  " Better for terminal
  set background=
  color tfs
endif

"
" @autocommands
"

if !exists("autocommands_loaded")
  let autocommands_loaded = 1

  " Automatically scale windows when resizing terminal
  autocmd VimResized * :wincmd =

  "" OmniCompletion function
  autocmd Filetype * if &omnifunc == "" | setl omnifunc=syntaxcomplete#Complete | endif

  " When saving vimrc source it
  autocmd BufWritePost vimrc source $MYVIMRC

  " When saving doc file, regenerate tags
  autocmd BufWritePost ~/.vim/doc/* :helptags ~/.vim/doc

  " Automatically change curent directory to that of the file in the buffer
  "autocmd BufEnter * cd %:p:h

  " Auto save when losing focus
  "autocmd FocusLost * silent! wa

  " Additional security for encrypted files
  autocmd BufReadPost * if &key != "" | set noswapfile nowritebackup viminfo= nobackup noshelltemp history=0 secure | endif
endif

"
" @filetype
"

filetype on
filetype plugin on
filetype indent on

if !exists("filetype_autocommands_loaded")
  let filetype_autocommands_loaded = 1

  augroup filetypeaucmds
    " Disable auto commenting
    autocmd FileType * setl formatoptions-=c formatoptions-=r formatoptions-=o

    " Makefile needs tabs
    autocmd BufNewFile,BufRead Makefile setl noet

    " Text files
    autocmd BufNewFile,BufRead *.md,*.txt,*.wiki setl tw=78 infercase nofoldenable

    " Enable spellchecking for git commits
    autocmd FileType gitcommit setlocal spell

    " Better go to link mapping
    autocmd FileType help nnoremap <buffer> <CR> <C-]>

    " Use indent folding for html
    autocmd BufNewFile,BufRead *.html setl foldmethod=indent foldlevel=3

    " Disable folding for css
    autocmd BufNewFile,BufRead *.css setl nofoldenable

    " Change foldlevel for php
    autocmd BufNewFile,BufRead *.php,*.theme,*.module,*.inc setl ft=php foldlevel=0

    " Force filetype for nginx site confs
    autocmd BufRead,BufNewFile /etc/nginx/sites-*/* setfiletype conf

    " Don’t want <:> to match for HTML because of closetag plugin
    au FileType html let b:delimitMate_matchpairs = "(:),[:],{:}"

    " Set tidal filetype back to haskell
    au FileType tidal setl ft=haskell

    " Open TOC when opening markdown file
    "fun! s:Toc()
    "  if &filetype == 'markdown'
    "    :Toc
    "  endif
    "endfunction
    "autocmd BufReadPost *.m* call s:Toc()
    "autocmd BufWinEnter *.m* call s:Toc()

    " Move between paragraphs
    fun! CustomParagraphMove()
      if exists('b:noCustomParagraphMoves')
          return
      endif
      nnoremap <buffer> J }
      nnoremap <buffer> K {
    endfun
    autocmd BufNewFile,BufRead * call CustomParagraphMove()
    autocmd FileType text let b:noCustomParagraphMoves=1
  augroup END
endif

