# ViM conf v4

## What?

Plugins are managed by [vim-plug](https://github.com/junegunn/vim-plug).

You need to install syntax checkers by yourself :

    brew install \
      homebrew/php/php-code-sniffer \
      homebrew/php/phpmd \
      homebrew/php/phplint \
      tidy-html5
    npm install -g \
      jshint \
      PrettyCSS

## Notes

I've added variables that affect snippets behavior, this includes author name
and email, HTML lang attribute value and HTML tag closing style. Just search
for UltiSnips in the vimrc.
